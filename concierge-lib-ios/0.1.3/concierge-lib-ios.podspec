#
# Be sure to run `pod lib lint concierge-lib-ios.podspec --allow-warnings' to ensure this is a
# valid spec before submitting.
# run 'pod repo push 2denker-spec concierge-lib-ios.podspec --allow-warnings --verbose'
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'concierge-lib-ios'
  s.version          = '0.1.3'
  s.summary          = 'A short description of concierge-lib-ios.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

   s.homepage         = 'https://2denker.de'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Christian Denker' => 'christian@2denker.de' }
  s.source           = { :git => 'git@bitbucket.org:2denker/concierge-lib-ios.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'concierge-lib-ios/Classes/**/*.{h,m}'
  
  # s.resource_bundles = {
  #   'concierge-lib-ios' => ['concierge-lib-ios/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
  # s.xcconfig = { 'HEADER_SEARCH_PATHS' => '$(SDKROOT)/usr/include/libxml2' }
end
