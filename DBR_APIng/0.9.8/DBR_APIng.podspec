#
# Be sure to run `pod lib lint DBR_APIng.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'DBR_APIng'
  s.version          = '0.9.8'
  s.summary          = 'APIng Backend library'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = 'Backend library for iOS to connect to APIng'
#TODO: Add long description of the pod here.
#DESC

  s.homepage         = "http://www.2denker.de"
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Christian Bacaj' => 'bacaj@2denker.de' }
  s.source           = { :git => 'git@bitbucket.org:2denker/dbr-aping-ios.git', :tag => s.version.to_s }

  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

  s.source_files = 'DBR_APIng/Classes/**/*'
  #s.resource_bundles = {
  #  'DBR_APIng' => ['DBR_APIng/Assets/*.png']
  #}
  s.resources = "DBR_APIng/**/*.{png,json}"

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'AFOAuth2Manager'
  s.dependency 'AFNetworking'
  s.dependency 'SwiftyJSON'

end
