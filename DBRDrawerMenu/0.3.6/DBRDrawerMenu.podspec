#
# Be sure to run `pod lib lint DBRDrawerMenu.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "DBRDrawerMenu"
  s.version          = "0.3.6"
  s.summary          = "A short description of DBRDrawerMenu."

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!  
  s.description      = "Android link side menue. Using in CallaBike and Flinkster Apps"
  s.homepage         = "http://www.2denker.de"
  # s.screenshots    = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "Christian Denker" => "christian@2denker.de" }
  s.source           = { :git => "git@bitbucket.org:2denker/drawer-menu-ios.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '7.0'
  s.requires_arc = true

s.source_files = 'Pod/Classes/**/*.{h,m}'
#s.resources = 'Pod/Classes/**/*.xib'
#s.resources = 'Pod/Classes/**/*.{png,bundle,xib,nib}'
#s.resources = ["classes/DBRAccountTableViewController.xib"]
#  s.resource_bundles = {
#    'DBRDrawerMenu' => ['Pod/Assets/*.png']
#  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
