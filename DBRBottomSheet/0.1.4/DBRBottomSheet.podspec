#
# Be sure to run `pod lib lint DBRBottomSheet.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'DBRBottomSheet'
  s.version          = '0.1.4'
  s.summary          = 'A short description of DBRBottomSheet.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = "http://www.2denker.de"
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Christian Bacaj' => 'bacaj@2denker.de' }
  s.source           = { :git => "git@bitbucket.org:2denker/bottom-sheet-ios.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'
  s.source_files = 'DBRBottomSheet/Classes/**/*.{h,m}'
  s.resources = [ "DBRBottomSheet/Classes/DBRBottomSheetViewController.xib" ]
  #s.resource = "DBRBottomSheet/**/*.{png,bundle,xib,nib}"

  # s.resource_bundles = {
  #   'DBRBottomSheet' => ['DBRBottomSheet/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
