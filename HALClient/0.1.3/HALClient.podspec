#
# Be sure to run `pod lib lint HALClient.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'HALClient'
  s.version          = '0.1.3'
  s.summary          = 'A short description of HALClient.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = 'Provides a Objectiv-C Framework to connect to HAL Endpoint'
  s.homepage         = 'https://www.zweidenker.de'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Christian Denker' => 'christian@2denker.de' }
  s.source           = { :git => 'git@bitbucket.org:2denker/dbc-hal-ios.git', :tag => s.version.to_s }

  s.ios.deployment_target = '9.0'
  s.prefix_header_contents = '#import "DBRFlinkster-prefix.h"'

  s.source_files = 'HALClient/Classes/**/*'
  
  # s.resource_bundles = {
  #   'HALClient' => ['HALClient/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
